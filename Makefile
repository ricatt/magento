# Environment variables -------------------------------------------------------
SHELL:=/bin/bash
PHP?=$(shell command -v php)

# Path variables --------------------------------------------------------------
SRC_PATHS?=src

# Default target --------------------------------------------------------------
.PHONY: default
default: composer_install


# Test hooks for libraries to use ---------------------------------------------
.PHONY: require
require:

# Includes --------------------------------------------------------------------
include deploy/Composer.make