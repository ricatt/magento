# Composer targets ---------------------------------------------------------------------------------------
# Composer library must be included *before* any other library that uses composer
.PHONY: composer_install composer_install_production composer_update composer_require composer_autoload composer_selfupdate

COMPOSER_PATH?=src/
COMPOSER?=../deploy/composer.phar

require: composer_configuration composer_selfupdate

composer_install:
	cd $(COMPOSER_PATH) && $(PHP) $(COMPOSER) install --prefer-dist $(COMPOSER_PARAMS)

composer_install_production:
	cd $(COMPOSER_PATH) && $(PHP) $(COMPOSER) install --prefer-dist --no-dev --optimize-autoloader --ignore-platform-reqs $(COMPOSER_PARAMS)

composer_update:
	cd $(COMPOSER_PATH) && $(PHP) $(COMPOSER) update --prefer-dist $(COMPOSER_PARAMS)

composer_require:
	cd $(COMPOSER_PATH) && $(PHP) $(COMPOSER) require --prefer-dist --sort-packages $(COMPOSER_PARAMS)

composer_autoload:
	cd $(COMPOSER_PATH) && $(PHP) $(COMPOSER) dump-autoload

composer_validate:
	cd $(COMPOSER_PATH) && $(PHP) $(COMPOSER) validate --no-check-lock

composer_selfupdate:
	cd $(COMPOSER_PATH) && $(PHP) $(COMPOSER) self-update
