#!/bin/bash
cd src/

mkdir -p ~/.composer
echo "$COMPOSER_AUTH" > ~/.composer/auth.json
php ../deploy/composer.phar install --prefer-dist --no-dev --optimize-autoloader --ignore-platform-reqs
php ../deploy/composer.phar validate --no-check-lock