#!/bin/bash

cd src/

REMOTE_DIR='/var/www/topformula/releases'

PRIVKEY=`mktemp`
echo "$DEPLOY_SSH_PRIVATE" > "$PRIVKEY"

SSH="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"

TIMEOFCOMMIT=`git log -n 1 --format="%ct"`
LASTHASH=`git log -n 1 --format="%h"`
NEWVERSION=`date +"%Y%m%d%H%M%S" --date "@$TIMEOFCOMMIT"`-$LASTHASH

git fetch
ARCHIVE=`mktemp`
git archive $CI_BUILD_REF > $ARCHIVE
tar --delete -f "$ARCHIVE" dev #public/.htaccess.example
tar --append -f "$ARCHIVE" vendor


#####################################################
# The script that will be executed on the remote host
cat > handle_deploy.sh << EOL
echo "$CI_BUILD_REF" > version.txt
echo "CI_BUILD_REF=$CI_BUILD_REF" > version.env
chmod a+rwx logs
rm handle_deploy.sh
cp ../../env.php app/etc/env.php
cd ../..
rm current
ln -s releases/$NEWVERSION current
cd current
php bin/magento maintenance:enable
php bin/magento deploy:mode:set -s production
php bin/magento setup:upgrade
php bin/magento setup:di:compile
rm -rf var/di/* var/generation/* var/cache/* var/log/* var/page_cache/* var/session/* var/view_preprocessed/* /pub/static/frontend/Provideit/Topformula/*
php bin/magento setup:static-content:deploy -t Provideit/Topformula -f
php bin/magento cache:flush
chgrp -R www-data .
php bin/magento maintenance:disable
EOL
####################################################
chmod +x handle_deploy.sh
tar --append -f "$ARCHIVE" handle_deploy.sh
rm handle_deploy.sh

gzip -c "$ARCHIVE" | $SSH -i "$PRIVKEY" $DEPLOY_PROD_USER@$DEPLOY_PROD_HOST "cd $REMOTE_DIR; mkdir $NEWVERSION; cd $NEWVERSION; gzip -d | tar x; ./handle_deploy.sh"

rm "$ARCHIVE"
rm "$PRIVKEY"
