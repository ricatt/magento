<?php
/**
 * Created by PhpStorm.
 * User: ricatt
 * Date: 2019-01-25
 * Time: 18:33
 */

namespace Ricatt\MediaPlayer\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Widget\Block\BlockInterface;

class MediaPlayer extends Template implements BlockInterface {
    protected $_template = 'widget/player.phtml';

    public function __construct(Context $context, array $data = []) {
        parent::__construct($context, $data);
        var_dump('Media player');
    }

    public function qwerty() {
        return 'qwerty';
    }
}